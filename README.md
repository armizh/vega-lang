# vega-lang
Modern and sugarfree low-level programming language. This implementation transpile a VC project to C code.

## Variables

### Examples
```
let str: u8* = "apple";
```
Since `"apple"` is undisputedly a char array you can omit the type definition and write
```
let str = "apple";
```
But if you want to store `"apple"` as a string you have to specify it
```
let str: string = "apple";
// OR
let str = string("apple");
```

## Functions

### Examples
```
fn sum(a: i64, b: i64): i64 {
  return a + b;
}
```
But if a function's logic can be written in one line you could do
```
fn sum(a: i64, b: i64): i64 => a + b;
```

## Classes
A class is a structure with properties and methods that can be used as an abstraction.

### Self vs self
The `Self` keyword refers to the class while `self` refers to the class instance. Technically speaking, `self` is a `Self&`.

### Properties
A property is a variable that belongs to a class instance. They are classified by their privacy:
- Public: can be read and written outside the instance it belongs to.
- Read-only: can only be read outside the instance it belongs to.
- Private: can't be read nor written outside the instance it belongs to.

For default, all properties are read-only.

### Examples
```
class A {
  readonly a: i32;  // or simply `a: i32;` since properties are readonly for default
  public b: i32;
  private c: i32; 
}
```

### Implicit conversion


### Examples
```
class Person {
  name: string;
  email: string

  constructor(name: string) {
    self.name = name;
  }
}
```

## Memory management
Both functions are defined on `runtime.vc`.

### Alloc
Allocates memory. It's defined as follows:

```
fn alloc<T>(n: u64): void*;
```

### Free
Frees memory. It's defined as follows:

```
fn free(ptr: void*);
```

### Examples

```
let buffer = alloc u8[256];

...

free buffer;
```

#### Notes
- `alloc type[n]` is a syntactic sugar for `alloc<type>(n)`.
- `free ptr` is a syntactic sugar for `free(ptr)`.