using System.Collections.Generic;
using System.Linq;

namespace Vega {
  public class Reference {
    static public char Parse(char c) {
      if(char.IsWhiteSpace(c)) return ' ';
      else if(char.IsLetterOrDigit(c)) return 'w';
      else return c;
    }

    static public IEnumerable<char> Parse(string code) => code.Select(c => Parse(c));
  }
}