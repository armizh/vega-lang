using System;
using System.IO;
using System.Linq;

namespace Vega {
  public class Program {
    static public void Main(string[] args) {
      var file = File.ReadAllLines(args[0]);

      foreach(string line in file) {
        var refs = Reference.Parse(line);

        Console.WriteLine(string.Concat(refs));
      }
    }
  }
}